﻿using UnityEngine;
using System.Collections;

namespace PROne
{

    public class Player : Entity, IAttack
    {

        [SerializeField]
        private Enemy enemyEntity;

        public Enemy EnemyEntity

        {

            set { enemyEntity = value; }
            get { return enemyEntity; }


        }


        [SerializeField]
        private int currentHealth = 100;

        public int CurrentHealth
        {

            set { currentHealth = value; }
            get { return currentHealth; }


        }

        [SerializeField]
        private int attackAmount = 55;

        public int AttackAmount
        {

            set { attackAmount = value; }
            get { return attackAmount; }


        }

        [SerializeField]
        private bool isAlive = true;

        public bool IsAlive
        {

            set { isAlive = value; }
            get { return isAlive; }


        }

        public void AttackEnemy(Enemy enemy, int amount)
        {
            if (enemy.IsAlive)
            {
                enemy.Damage(amount);
            }
        }




        private void Start()
        {
            Log(EntityName);
            Log(currentHealth);
            Log(attackAmount);
            AttackEnemy(enemyEntity, attackAmount);
        }

    }

}
