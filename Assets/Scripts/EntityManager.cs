﻿using UnityEngine;
using System.Collections;

namespace PROne
{

    public class EntityManager : MonoBehaviour, ILog
    {

        public void Log(object message)
        {
            Debug.Log(message);
        }

        public Entity[] entities;               

        private void Start()
        {
            Log("The entity names are being registered.");

            for (int i = 0; i < entities.Length; i++)
            {
                Log(i + "Humanoid" + entities[i].EntityName);
            }
        }

    }

}
