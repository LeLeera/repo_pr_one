﻿using UnityEngine;
using System.Collections;

namespace PROne
{

    public class Enemy : Entity, IDamagable
    {

        [SerializeField]
        private int currentHealth = 1000;

        public int CurrentHealth
        {

            set { currentHealth = value; }
            get { return currentHealth; }


        }

        [SerializeField]
        private int attackAmount = 10;

        public int AttackAmount
        {

            set { attackAmount = value; }
            get { return attackAmount; }


        }

        [SerializeField]
        private bool isAlive = true;

        public bool IsAlive
        {

            set { isAlive = value; }
            get { return isAlive; }


        }


        public void Damage(int amount)
        {
            if (currentHealth > 0)                  
            {
                currentHealth -= amount;

                if (currentHealth <= 0)                 
                {
                    currentHealth = 0;

                    isAlive = false;

                    Log(EntityName + currentHealth);

                    return;

                }

            }

            if (currentHealth <= 0)                 
            {
                currentHealth = 0;

                isAlive = false;

                Log(EntityName + currentHealth);

            }

            Log(EntityName + " health has been brought down to " + currentHealth);  

        }



    }    


}
