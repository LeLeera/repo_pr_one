﻿using UnityEngine;
using System.Collections;

namespace PROne
{
    public interface IDamagable
    {

        // Methodsignature with one argument

        void Damage(int amount);                    //amount = amount of damage 

    }
}
