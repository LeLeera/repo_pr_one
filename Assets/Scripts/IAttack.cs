﻿using UnityEngine;
using System.Collections;

namespace PROne
{
    public interface IAttack
    {

        // Methodsignature with two arguments

        void AttackEnemy(Enemy enemy, int amount);  //Enemy  = which enemy is being attacked
                                                    //amount = amount of attack damage

    }
}
